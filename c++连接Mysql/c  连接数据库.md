# 连接MySQL

```c++
#include <WinSock.h>			//连接模式库
#include <mysql.h>//包含MySQL的API函数的头文件
#pragma comment(lib,"libmysql.lib") //导入libmysql.lib库
MYSQL mysql;//声明数据库变量
#define HOST "localhost"		//数据库服务名 
#define USERNAME "root"			//用户名 
#define PASSWORD "g1234"		//密码 
#define DATABASE "spj"			//连接目标数据库 
int main() {
	MYSQL mysql;	//声明数据库变量 
	mysql_init(&mysql);	//初始化数据库连接 
	mysql_options(&mysql, MYSQL_SET_CHARSET_NAME, "gbk");	//设置编码方式 
	if(mysql_real_connect(&mysql,HOST,USERNAME,PASSWORD,DATABASE, 3306, NULL, 0)){	//连接数据库
		//连接成功
		printf("连接成功！\n");
	}else{
		//连接失败
		printf("连接失败！\n");
	}
	mysql_close(&mysql);//关闭数据库连接 
	return 0;
}
```

> ==MYSQL *mysql_init(MYSQL *mysql)==:
>
> 分配或初始化一个适用于 mysql_real_connect() 的 MYSQL 对象，用于连接 MySQL 服务端。如果传入 NULL，会自动分配一个 MySQL 对象。

> ==MYSQL *mysql_real_connect(MYSQL *mysql,
>                    const char *host,
>                    const char *user,
>                    const char *passwd,
>                    const char *db,
>                    unsigned int port,
>                    const char *unix_socket,
>                    unsigned long client_flag)==:
>
> 尝试建立到主机上运行的 MySQL 服务器的连接。在执行任何其他需要有效 MYSQL 连接处理程序结构的 API 函数之前，客户端程序必须成功连接到服务器。

> ==void mysql_close(MYSQL *mysql)==:
>
> 关闭先前打开的连接。 如果处理程序是由 mysql_init() 或 mysql_connect() 自动分配的，则 mysql_close() 还会释放 mysql 指向的连接处理程序。不要在处理程序关闭后使用它。

> ==int mysql_set_character_set(MYSQL *mysql, const char *csname)==:
>
> 该函数用于设置当前连接的默认字符集。字符串 csname 指定一个有效的字符集名称。

# 数据的插入

```c++
int insert()
{
	char   sno[10];
	char   sname[10];
	char   status[10];
	char   city[10];
	char strquery[100] = "insert into s values('";
	//char yn[2];
	while (1)
	{
        //输入sql语句
		cout << "请输入供应商代码(sno)(如：S1):";
		cin >> sno;
		strcat_s(strquery, sno);
		strcat_s(strquery, "','");		//插入sql语句
		cout << "请输入供应商姓名(SNAME)(如：精益):";
		cin >> sname;
		strcat_s(strquery, sname);
		strcat_s(strquery, "','");
		cout << "请输入供应商状态(STATUS)(如：20):";
		cin >> status;
		strcat_s(strquery, status);
		strcat_s(strquery, "','");
		cout << "请输入供应商所在城市(CITY)(如：天津):";
		cin >> city;
		strcat_s(strquery, city);
		strcat_s(strquery, "');");
        //处理sql语句
		if (mysql_query(&mysql, strquery) == 0) {		
			cout << "插入成功！" << endl;
		}
		else {
			string error(mysql_error(&mysql));//无错误返回空串
			cout << "Error:" << error << endl;
			/cout << "Error:插入失败！" << endl;
		}
		break;
	}
	return 0;
}
```

> ==int mysql_query(MYSQL *mysql, const char *stmt_str)==：
>
> 执行以 null 结尾的字符串 stmt_str 指向的 SQL 语句。返回 0 表示成功，否则执行出错。

> ==String mysql_error(&mysql)==:
>
> 返回上一个 MySQL 操作产生的文本错误信息

# 数据的修改

```c++
int update()
{
	char   sno[10];
	char   sname[10];
	char   status[10];
	char   city[10];
    //输入sql语句
	cout << "请输入所需要修改的供应商代码(sno)(如：S1):";
	cin >> sno;
	char strquery[100] = "UPDATE s SET sname='";
	char yn[2];
	cout << "请输入供应商姓名(SNAME)(如：精益):";
	cin >> sname;
	strcat_s(strquery, sname);
	strcat_s(strquery, "', status='");
	cout << "请输入供应商状态(STATUS)(如：20):";
	cin >> status;
	strcat_s(strquery, status);
	strcat_s(strquery, "', city='");
	cout << "请输入供应商所在城市(CITY)(如：天津):";
	cin >> city;
	strcat_s(strquery, city);
	strcat_s(strquery, "' WHERE sno='");
	strcat_s(strquery, sno);
	strcat_s(strquery, "';");
    //执行sql语句
	mysql_query(&mysql, strquery); 
	int num(mysql_affected_rows(&mysql));
	string error(mysql_error(&mysql));
	if (num > 0 && error == "")
	{
		cout << "修改" << sno << "成功！\n";
	}
	else
	{
		cout << "修改" << sno << "失败！" << endl;
		if (error != "")//给出修改不成功的原因 
		{
			cout << "Error:" << mysql_error(&mysql) << endl;
		}
		else {
			cout << "Error:不存在(" << sno << ")代码号" << endl;
		}
	}
	return 0;

}
```

> ==int mysql_affected_rows(&mysql)==:
>
> 用于获取最近一条SQL语句执行后所影响的行数

# 数据的删除

```c++
int deleteData()
{
	char   yn[2];
	char   sno[10];
	char   sname[10];
	char   status[10];
	char   city[10];

	MYSQL_ROW row;
	cout << "请输入所要删除零件代码(SNO):\n"; 
	cin >> sno;
	char strquery[100] = "DELETE FROM s WHERE sno='";
	strcat_s(strquery, sno);
	strcat_s(strquery, "';");
	mysql_query(&mysql, strquery); //执行sql语句 
	int num(mysql_affected_rows(&mysql));
	string error(mysql_error(&mysql));
	if (num > 0 && error == "")
	{
		cout << "删除" << sno << "成功！\n";
	}
	else
	{
		cout << "删除" << sno << "失败！" << endl;
		if (error != "")//给出删除不成功的原因 
		{
			cout << "Error:" << mysql_error(&mysql) << endl;
		}
		else {
			cout << "Error:不存在(" << sno << ")代码号" << endl;
		}
	}

	return 0;
}
```

# 数据的查询

```c++
int select()
{
	char   sno[10];
	char   sname[9];
	char   status[9];
	char   city[9];
	MYSQL_ROW row;
	char strquery[100] = "select sno,sname,status,city from s where sno like '%";
	printf("请输入SNO:");
	cin >> sno;
	strcat_s(strquery, sno);
	strcat_s(strquery, "%';");//模糊查询
	mysql_query(&mysql, strquery);
	MYSQL_RES* result = mysql_store_result(&mysql);
    
	int num_fields = mysql_field_count(&mysql);//l
	int flag = 0;
	while ((row = mysql_fetch_row(result)) != NULL)
	{
		if (flag == 0) {
			cout << "sno name status city  " << endl;
			flag = 1;
		}
		for (int i = 0; i < num_fields; i++)
		{
			switch (i)
			{
			case 0:cout << row[i] << "  ";
				strcpy_s(sno, row[i]);    break;
			case 1:cout << row[i] << "   ";
				strcpy_s(sname, row[i]);   break;
			case 2:cout << row[i] << "   ";
				strcpy_s(status, row[i]); break;
			case 3:cout << row[i];
				strcpy_s(city, row[i]);   break;
			}
		}
		cout << endl;
	}
	string error(mysql_error(&mysql));//无错误返回空串
	if (error != "")
	{
		cout << "Error:" << error;
	}
	if (flag == 0) {
		cout << "数据库无该信息！" << endl;
	}
	return 0;
}
```

> ==MYSQL_RES *mysql_store_result(MYSQL *mysql)==:
>
> 在调用 mysql_real_query() 或 mysql_query() 之后，必须为每个成功生成结果集的语句（SELECT、SHOW、DESCRIBE、EXPLAIN、CHECK TABLE 等）调用 mysql_store_result() 或 mysql_use_result()。在完成结果集后，您还必须调用 mysql_free_result()。

> ==int mysql_field_count(&mysql)==:
>
> 获取结果中列的个数

> ==MYSQL_ROW mysql_fetch_row(MYSQL_RES *result)==:
>
> 检索结果集的下一行。当在 mysql_store_result() 或 mysql_store_result_nonblocking() 之后使用时，如果没有更多的行要检索，mysql_fetch_row() 将返回 NULL。当在 mysql_use_result() 之后使用时，如果没有更多的行可检索或发生错误，mysql_fetch_row() 将返回 NULL。

# 查看所有数据

```
int display() {
	char   sno[10];
	char   sname[9];
	char   status[9];
	char   city[9];
	//mysql_query(&myconnt, "SET NAMES 'GB2312' ");
	MYSQL_ROW row;
	char strquery[100] = "select sno,sname,status,city from s";
	mysql_query(&mysql, strquery);
	MYSQL_RES* result = mysql_store_result(&mysql);
	int num_fields = mysql_field_count(&mysql);
	int flag = 0;
	while ((row = mysql_fetch_row(result)) != NULL)
	{
		if (flag == 0) {
			cout << "sno name status city  " << endl;
			flag = 1;
		}
		for (int i = 0; i < num_fields; i++)
		{
			switch (i)
			{
			case 0:cout << row[i] << "  ";
				strcpy_s(sno, row[i]); break;
			case 1:cout << row[i] << "   ";
				strcpy_s(sname, row[i]); break;
			case 2:cout << row[i] << "   ";
				strcpy_s(status, row[i]); break;
			case 3:cout << row[i];
				strcpy_s(city, row[i]); break;
			}
		}
		cout << endl;
	}
	string error(mysql_error(&mysql));//无错误返回空串
	if (error != "")
	{
		cout << "Error:" << error;
	}
	return 0;
}

```

